
from wish import Wish
import pickle
from datetime import datetime, timedelta
import sys, os


class WishProgram:

	def __init__(self):
		self.wishlist = []
		print('\nWelcome (back) to your wishlist!')
		self.main()		

	def list_wishes(self):
		print('\n')
		for i, wish in enumerate(self.wishlist):
			time_since =  round((datetime.now() - wish.created).seconds / 60,0)

			if self.wishlist[i].completed:
				print(str(i+1) + '. ' + wish.text + ' -- Completed on ' + self.wishlist[i].completion_time.strftime("%Y-%m-%d %H:%M") + '.')
			else:
				print(str(i+1) + '. ' + wish.text + ' -- Added ' + str(int(time_since)) + ' minutes ago.')

		idx = input('Please input a wish number (or ESC to go back): ')

		if idx.isnumeric():
			idx = int(idx)-1
			if idx <= len(self.wishlist):
				print("\nSelected '" + self.wishlist[idx].text + '"\n')

				inp = input('C - Complete, D - Delete (or ESC to go back): ')

				if inp.upper() == 'C':
					self.wishlist[idx].completed = True
					self.wishlist[idx].completion_time = datetime.now()
					print('Wish completed!')

				if inp.upper() == 'D':
					del self.wishlist[idx]
					print('Wish deleted')


	def new_wish(self):
		info = input('Please add a wish: ')
		new_wish = Wish(info)
		self.wishlist.append(new_wish)

	def stats(self):
		pass

	def save(self):
		with open('wishes.pickle', 'wb') as handle:
	  		pickle.dump(self.wishlist, handle)
	  		print ("x-----------------------------x")
	  		print ("         Stats saved.")
	  		print ("x-----------------------------x")

	def load(self):
		with open('wishes.pickle', 'rb') as handle:
			res = pickle.load(handle)
			print ("x-----------------------------x")
			print ("         Stats loaded.")
			print ("x-----------------------------x")

			self.wishlist = res

	def quit(self):
		self.save()
		quit()

	def main(self):

		self.load()

		while(1):
			print('\n')
			print("L - List Wishes, N - New Wish, S - Stats, SS - Save, LL - Load, Q - Quit.")

			entry = {'L':self.list_wishes,
			'N':self.new_wish,
			'S':self.stats,
			'SS':self.save,
			'LL':self.load,
			'Q':self.quit
			}

			inp = input('Please choose an option: ')

			try:
				entry[inp.upper()]()
			except KeyError as k_err:
				print('Invalid selection, please try again.\n')


wishprog = WishProgram()