from datetime import datetime


# Wish object structure

class Wish():

	def __init__(self,wish,wish_priority = 0):

		self.created = datetime.now()
		self.text = wish
		self.completed = False
		self.completion_time = datetime.now()
